//
// Created by lh123 on 3/25/21.
//
#include <cstdio>
#include <iostream>
#include <mutex>
#include <thread>

#include <vector>

#include "gtest/gtest.h"
using namespace std;

class Mutexlock{
 private:
  mutex* mu_;
 public:
  explicit Mutexlock(mutex* mu):mu_(mu){
    this->mu_->lock();
  }
  ~Mutexlock(){
    this->mu_->unlock();
  }
  Mutexlock(const Mutexlock&) = delete;
  Mutexlock& operator=(const Mutexlock&) = delete;
};

class List {
 public:
  struct Node;
  List();
  bool Add(int v);
  bool Contain(int v);
  bool Delete(int v);
  string to_string();
  vector<int> getallval();
 private:
  Node* header;
};

struct List::Node{
  int val;
  Node* next;
  mutex mu;
  Node(){
    val=0;
    next=NULL;
  }
  explicit Node(int x){
    val=x;
    next=NULL;
  }
};

List::List() { header = new Node(); }
vector<int> List::getallval() {
  vector<int> result;
  Node* cur=header;
  while(cur!=NULL){
    result.push_back(cur->val);
    cur=cur->next;
  }
  return result;
}
bool List::Add(int x){
  Mutexlock lock(&header->mu);
  Node *cur=header;
  while(cur->next!=NULL){
    if(cur->next->val>=x){
      break;
    }
    cur=cur->next;
  }
  if(cur->next!=NULL&&cur->next->val==x){
    std::cout<<"can't add duplicated value";
    return false;
  }
  Node* pre=new Node(x);
  pre->next=cur->next;
  cur->next=pre;
  return true;
}
bool List::Contain(int x) {
  Mutexlock lock(&header->mu);
  Node* cur=header;
  while(cur!=NULL){
    if(cur->val==x){
      return true;
    }
    cur=cur->next;
  }
  return false;
};
bool List::Delete(int v) {
  Mutexlock lock(&header->mu);
  int find=0;
  if(header->val==v){
    find=1;
    header=header->next;
    return true;
  }
  Node* temp=header;
  Node* pre=NULL;
  while(temp->next!=NULL){
    pre=temp;
    temp=temp->next;
    if(temp->val==v){
      find=1;
      pre->next=temp->next;
      delete temp;
      break;
    }
  }
  if(find){
    return true;
  }
  else{
    return false;
  }
}
string List::to_string() {
  string ans = "Header";
  Node* p = header->next;
  while (p != nullptr) {
    ans += " -> " + std::to_string(p->val);
    p = p->next;
  }
  return ans;
}
TEST(List, Add) {
List* l = new List();
ASSERT_EQ(l->Add(1), true);
ASSERT_EQ(l->Add(2), true);
ASSERT_EQ(l->Add(-2323), true);
ASSERT_EQ(l->Add(2), false);
cout << l->to_string() << endl;
ASSERT_EQ(l->Contain(1), true);
ASSERT_EQ(l->Contain(2), true);
ASSERT_EQ(l->Contain(-2323), true);
ASSERT_EQ(l->Contain(12313), false);
ASSERT_EQ(l->Contain(34), false);
delete l;
}
TEST(List, Delete) {
List* l = new List();
l->Add(1);
l->Add(2);
l->Add(-2323);
l->Add(123);
l->Add(-1231234);
cout << l->to_string() << endl;
l->Delete(1);
l->Delete(2);
l->Delete(4);
l->Delete(-1231234);
ASSERT_EQ(l->Contain(1), false);
ASSERT_EQ(l->Contain(2), false);
ASSERT_EQ(l->Contain(4), false);
ASSERT_EQ(l->Contain(-2323), true);
ASSERT_EQ(l->Contain(123), true);
cout << l->to_string() << endl;
delete l;
}
TEST(ListMultiThread, Add) {
int thread_number = 100;
int insert_num = 100;
List* l_add = new List();
vector<thread> add_thread;
add_thread.reserve(thread_number);
for (int i = thread_number - 1; i >= 0; i--) {
add_thread.emplace_back(
[](List* l, int i, int insert_num) {
for (int k = i * insert_num; k < i * insert_num + insert_num; k++) {
l->Add(k);
}
},
l_add, i, insert_num);
}
for (auto& t : add_thread) {
t.join(); // wait here
}
// Check
for (int i = 1; i < insert_num * thread_number; i++) {
ASSERT_EQ(l_add->Contain(i), true);
}
delete l_add;
}
TEST(ListMultiThread, Delete) {
int thread_number = 10;
int insert_num = 100;
List* l_delete = new List();
for (int i = 1; i < insert_num * thread_number; i++) {
ASSERT_EQ(l_delete->Add(i), true);
}
vector<thread> add_thread;
add_thread.reserve(thread_number);
for (int i = 1; i < thread_number; i++) {
  add_thread.emplace_back(
    [](List* l, int i, int insert_num) {
    for (int k = i * insert_num; k < i * insert_num + insert_num; k++) {
  ASSERT_EQ(l->Delete(k), true);
    }
    },
  l_delete, i, insert_num);
}
for (auto& t : add_thread) {
t.join();
}
// Check
for (int i = insert_num; i < insert_num * thread_number; i++) {
ASSERT_EQ(l_delete->Contain(i), false);
}
delete l_delete;
}
TEST(ListMultiThread, InsertAndDelete){
int thread_number = 10;
int insert_num = 100;
List* l_ad = new List();
vector<thread> add_thread;
add_thread.reserve(thread_number);
for (int i = 0; i < thread_number; i++) {
add_thread.emplace_back(
[](List* l, int i, int insert_num) {
for (int k = i * insert_num; k < i * insert_num + insert_num; k++) {
ASSERT_EQ(l->Add(k), true);
ASSERT_EQ(l->Delete(k), true);
}
},
l_ad, i, insert_num);
}
for (auto& t : add_thread) {
t.join();
}
vector<thread>add_thread2;
add_thread2.reserve(thread_number);
for (int i = 1; i < thread_number; i++) {
add_thread2.emplace_back(
[](List* l, int i, int insert_num) {
for (int k = 1; k < insert_num ; k++) {
l->Add(k);
l->Delete(k);
}
},
l_ad, i, insert_num);
}
for (auto& t : add_thread2) {
t.join();
}
// Check
for (int i = 1; i < insert_num * thread_number; i++) {
ASSERT_EQ(l_ad->Contain(i), false);
}
}
int main() {
  testing::InitGoogleTest();
  return RUN_ALL_TESTS();
}
