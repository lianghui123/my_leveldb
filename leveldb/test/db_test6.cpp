//
// Created by lh123 on 4/1/21.
//
#define SHOW
#include <iostream>
#include <vector>
#include "gtest/gtest.h"
struct ReadOption {
  int version;
  ReadOption() { version = -1; }
};
enum Op { Value = 0, DELETE = 1 };
class List {
 public:
  struct Node;
  List();
  bool Put(int key, int value);
  bool Delete(int key);
  bool Get(ReadOption r, int key, int& value);
  int GetSnapshot();
  std::string ToString();
 private:
  Node* header;
};
struct List::Node {
  Op op;
  int key;
  int value;
  int version;
  Node* next;
  Node() {
    key = 0;
    value = 0;
    version = 0;
    next = nullptr;
  }
  explicit Node(int k, int v, int version, Op op)
      : key(k), value(v), version(version), next(nullptr), op(op) {}
// TODO:
// 如果key不相同，则按照key从⼩到⼤排序
// 如果key相同，则按照version从⼤到⼩排序
  bool operator<(const Node& b) const {
    if(this->key<b.key){
      return true;
    }
    if(this->key==b.key&&this->version>b.version) {
      return true;
    }
    return false;
  }
};
List::List() { header = new Node(); }
// TODO: 插⼊⼀个新节点，版本号为最新版本
bool List::Put(int key, int value) {
  int _version=header->version;
  Node* temp=new Node(key,value,_version+1,Op::Value);
  Node* cur=header;
  while(cur->next!=NULL&&*(cur->next)<*temp){
    cur=cur->next;
  }
  temp->next=cur->next;
  cur->next=temp;
  header->version++;
  return true;
}
// TODO: ReadOption内传⼊版本号，
// 返回值存在value，如果没有找到对应的节点则返回false
bool List::Get(ReadOption r, int key, int& value) {
  int latest_version=r.version;
  if(latest_version<0){
    latest_version=header->version;
  }
  Node* cur=header;
  Node *temp=new Node(key,0,latest_version,Value);
  while(cur->next!=NULL&&*(cur->next)<*temp){
    cur=cur->next;
  }
  if(cur->next->op==Op::DELETE||!cur->next||cur->next->key!=key){
    return false;
  }
  value=cur->next->value;
  delete temp;
  return true;
}
// TODO: 插⼊⼀个操作为删除的节点
bool List::Delete(int key) {
  int latest_version=header->version;
  Node* temp=new Node(key,0,latest_version+1,Op::DELETE);
  Node* cur=header;
  while(cur->next!=NULL&&*(cur->next)<*temp){
    cur=cur->next;
  }
  temp->next=cur->next;
  cur->next=temp;
  header->version++;
  return true;

// 与Put唯⼀的区别就是操作为Delete
}
// TODO: 返回当前的版本号
int List::GetSnapshot() {
  return header->version;
}
std::string List::ToString() {
  std::string ans;
  Node* p = header->next;
  while (p != nullptr) {
    if (p->op == Value) {
      ans += "Key: " + std::to_string(p->key) +
             ", Version: " + std::to_string(p->version) +
             ", Value: " + std::to_string(p->value) + "\n";
    } else {
      ans += "Key: " + std::to_string(p->key) +
             ", Version: " + std::to_string(p->version) + ", Delete\n";
    }
    p = p->next;
  }
  ans += "------------------------PRINT------------------------\n";
  return ans;
}
TEST(Node, Compare) {
auto* c = new List::Node(-2, -2, 3, Value);
auto* d = new List::Node(1, 11, 4, DELETE);
auto* a = new List::Node(1, 1, 1, Value);
auto* b = new List::Node(2, 2, 2, Value);
ASSERT_LT(*a, *b);
ASSERT_LT(*d, *a);
ASSERT_LT(*c, *a);
}
/**
TEST(List, PutAndGet) {
List* l = new List();
ASSERT_TRUE(l->Put(1, 1));
ASSERT_TRUE(l->Put(2, 2));
ASSERT_TRUE(l->Put(-2, -2));
ASSERT_TRUE(l->Put(23, 2323));
#ifdef SHOW
std::cout<<l->ToString();
#endif
int value;
ASSERT_TRUE(l->Get(ReadOption(), 1, value));
ASSERT_EQ(value, 1);
ASSERT_TRUE(l->Get(ReadOption(), 2, value));
ASSERT_EQ(value, 2);
ASSERT_TRUE(l->Get(ReadOption(), -2, value));
ASSERT_EQ(value, -2);
ASSERT_TRUE(l->Get(ReadOption(), 23, value));
ASSERT_EQ(value, 2323);
// 这⾥覆盖Key=1的数据，读取出来的value应该是6
ASSERT_TRUE(l->Put(2, 6));
ASSERT_TRUE(l->Get(ReadOption(), 2, value));
ASSERT_EQ(value, 6);
// 查找⼀个不存在的数据，返回false
ASSERT_FALSE(l->Get(ReadOption(), 20000, value));
#ifdef SHOW
std::cout<<l->ToString();
#endif
delete l;
}*/
TEST(List, PutAndGet) {
  List* l = new List();
  ASSERT_TRUE(l->Put(1, 1));
  ASSERT_TRUE(l->Put(2, 2));
  ASSERT_TRUE(l->Put(-2, -2));
  ASSERT_TRUE(l->Put(23, 2323));
#ifdef SHOW
  std::cout<<l->ToString();
#endif
  int value;
  ASSERT_TRUE(l->Get(ReadOption(), 1, value));
  ASSERT_EQ(value, 1);
  ASSERT_TRUE(l->Put(2, 6));
  ASSERT_TRUE(l->Get(ReadOption(), 2, value));
  ASSERT_EQ(value, 6);
#ifdef SHOW
  std::cout<<l->ToString();
#endif
  delete l;
}
TEST(List, Delete) {
List* l = new List();
l->Put(1, 1);
l->Put(2, 2);
l->Put(4, 4);
l->Put(-2, -2);
l->Put(12, 12);
l->Put(-12, -12);
#ifdef SHOW
std::cout<<l->ToString();
#endif
l->Delete(1);
l->Delete(2);
l->Delete(4);
l->Delete(-12);
int ans;
ASSERT_FALSE(l->Get(ReadOption(), 1, ans));
ASSERT_FALSE(l->Get(ReadOption(), 2, ans));
ASSERT_FALSE(l->Get(ReadOption(), 4, ans));
ASSERT_FALSE(l->Get(ReadOption(), -12, ans));
ASSERT_TRUE(l->Get(ReadOption(), -2, ans));
ASSERT_TRUE(l->Get(ReadOption(), 12, ans));
#ifdef SHOW
std::cout<<l->ToString();
#endif
delete l;
}
// 这⾥复现实验⼀的场景
TEST(List, Snapshot) {
List* l = new List();
int ans;
int value_old[10] = {85, 90, 75, 60, 73, 86, 68, 88, 85, 94};
int value_new[10] = {80, 91, 80, 80, 70, 89, 70, 90, 88, 95};
// 把value_old数据插⼊，Key为0-9，并创建快照s1
for(int i=0;i<10;i++){
l->Put(i, value_old[i]);
}
ReadOption s1;
s1.version = l->GetSnapshot();
// ⽤value_new覆盖value_old的数据，创建快照s2
for(int i=0;i<10;i++){
l->Put(i, value_new[i]);
}
ReadOption s2;
s2.version = l->GetSnapshot();
// 删除所有的数据
for(int i=0;i<10;i++){
l->Delete(i);
}
// 通过s1版本号读取出的数据依然是s1版本的数据
for(int i=0;i<10;i++){
ASSERT_TRUE(l->Get(s1, i, ans));
ASSERT_EQ(ans, value_old[i]);
}
// 通过s2版本号读取出的数据依然是s2版本的数据
for(int i=0;i<10;i++){
ASSERT_TRUE(l->Get(s2, i, ans));
ASSERT_EQ(ans, value_new[i]);
}
// 这⾥Get之前删除的数据，查找失败
for(int i=0;i<10;i++){
ASSERT_FALSE(l->Get(ReadOption(), i, ans));
}
#ifdef SHOW
std::cout<<l->ToString();
#endif
}
int main() {
  testing::InitGoogleTest();
  return RUN_ALL_TESTS();
}
