//
// Created by lh123 on 3/11/21.
//
#include "leveldb/db.h"
#include <cstdio>
#include <iostream>

using namespace std;
using namespace leveldb;
int main() {
  DB *db = nullptr;
  Options op;
  op.create_if_missing = true;
  Status status = DB::Open(op, "testdb", &db);
  assert(status.ok());

  freopen("../test/input1", "r", stdin);
  string k, v;
  for(int i=0; i<10; i++){
    cin >> k >> v;
    db->Put(WriteOptions(), k, v);
  }
  fclose(stdin);

  ReadOptions op1;
  op1.snapshot = db->GetSnapshot();

  cin.clear();
  freopen("../test/input2", "r", stdin);
  string temp;
  for(int i=0; i<10; i++){
    cin >> k >> v;
    temp=k;
    db->Put(WriteOptions(), k, v);
  }
  fclose(stdin);

  Iterator* it = db->NewIterator(op1);
  string output[11];
  int i=0;
  freopen("../test/output_test","w",stdout);
  for(it->SeekToFirst();it->Valid();it->Next()){
    output[i]=it->key().ToString()+' '+it->value().ToString()+' ';
    i++;
    //cout<<it->key().ToString()<<' '<<it->value().ToString()<<endl;
  }

  Iterator* it1=db->NewIterator(ReadOptions());
  i=0;
  for(it1->SeekToFirst();it1->Valid();it1->Next()){
    output[i]+=it1->value().ToString();
    cout<<output[i]<<endl;
    i++;
  }
  fclose(stdout);
  delete it;
  delete it1;
  db->ReleaseSnapshot(op1.snapshot);
  return 0;
}

