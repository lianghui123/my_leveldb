
/*#include "leveldb/db.h"
#include <cstdio>
#include <iostream>

using namespace std;
using namespace leveldb;

int main() {
    DB* db = nullptr;
    Options op;
    op.create_if_missing = true;
    Status status = DB::Open(op, "testdb2", &db);
    assert(status.ok());
    status = db->Put(WriteOptions(), "1", "hobo");
    assert(status.ok());
    status = db->Put(WriteOptions(), "2", "hobo2");
    assert(status.ok());

    status = db->Put(WriteOptions(), "1", "hobo3");
    assert(status.ok());
    status = db->Delete(WriteOptions(), "1");
    assert(status.ok());

    delete db;
    return 0;
}*/

#include <iostream>
#include "leveldb/db.h"
namespace leveldb{
void GetDataWithPosition(){
  DB *db = nullptr;
  Options op;
  op.create_if_missing = true;
  Status status = DB::Open(op, "testdb_read_sstable", &db);
  assert(status.ok());
  for(int i=0;i<5;i++){
    db->Put(WriteOptions(), "00"+std::to_string(i), "00"+std::to_string(i));
  }
  std::string res;
  std::string pos;
  for(int i=0;i<5;i++){
    Status s = db->GetWithPosition(ReadOptions(),"00"+std::to_string(i), &res, &pos);
    s.ok() ? std::cout<<pos<<std::endl : std::cout<<"err"<<std::endl;
  }
  db->Write(WriteOptions(), nullptr);
  for(int i=0;i<5;i++){
    Status s = db->GetWithPosition(ReadOptions(),"00"+std::to_string(i), &res, &pos);
    s.ok() ? std::cout<<pos<<std::endl : std::cout<<"err"<<std::endl;
  }
  db->CompactRange(nullptr, nullptr);
  for(int i=0;i<5;i++){
    Status s = db->GetWithPosition(ReadOptions(),"00"+std::to_string(i), &res, &pos);
    s.ok() ? std::cout<<pos<<std::endl : std::cout<<"err"<<std::endl;
  }
  for(int i=5;i<10;i++){
    db->Put(WriteOptions(), "00"+std::to_string(i), "00"+std::to_string(i));
  }
  for(int i=5;i<10;i++){
    Status s = db->GetWithPosition(ReadOptions(),"00"+std::to_string(i), &res, &pos);
    s.ok() ? std::cout<<pos<<std::endl : std::cout<<"err"<<std::endl;
  }
  delete db;
}
}
int main(){
  leveldb::GetDataWithPosition();
  return 0;
}

